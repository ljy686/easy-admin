package com.mars.module.system.mapper;

import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysUserPost;

/**
 * 用户岗位关联表
 *
 * @author 源码字节-程序员Mars
 */
public interface SysUserPostMapper extends BasePlusMapper<SysUserPost> {


    /**
     * 删除用户岗位关联关系
     *
     * @param userId 用户ID
     */
    void deleteByUserId(Long userId);

}
