package com.mars.module.system.service;

import com.mars.common.request.sys.ResourceInfoRequest;
import com.mars.common.request.sys.SysMenuQueryRequest;
import com.mars.common.response.PageInfo;
import com.mars.module.system.entity.ResourceInfo;

import java.io.IOException;

/**
 * 系统菜单
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-08-23 09:45:22
 */
public interface IResourceInfoService {


    /**
     * 获取菜单列表
     *
     * @param request request
     * @return PageVo<ResourceInfo>
     */
    PageInfo<ResourceInfo> pageList(ResourceInfoRequest request);

    /**
     * 数据采集
     *
     * @param type 类型
     * @param page 采集页数
     */
    void fetchData(Integer type, Integer page) throws IOException;

}
