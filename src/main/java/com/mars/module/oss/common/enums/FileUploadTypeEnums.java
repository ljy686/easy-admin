package com.mars.module.oss.common.enums;

/**
 * @author Hp
 */

public enum FileUploadTypeEnums {
    /**
     * 阿里云文件上传
     */
    ALI_OSS("alioss", "阿里云文件上传"),

    /**
     * minio
     */
    MINIO("minio", "minio文件上传"),

    /**
     * 本地文件上传
     */
    LOCAL("local", "本地文件上传");

    private String type;
    private String desc;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    FileUploadTypeEnums() {
    }

    FileUploadTypeEnums(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static FileUploadTypeEnums getFileEnumsByType(String type) {
        FileUploadTypeEnums[] values = FileUploadTypeEnums.values();
        for (FileUploadTypeEnums enums : values) {
            if (enums.getType().equals(type)) {
                return enums;
            }
        }
        return null;
    }
}
