package com.mars.module.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Hp
 */
public interface IUploadService {

    /**
     * 文件上传
     *
     * @param file 文件
     * @return String
     */
    String upload(MultipartFile file) throws Exception;

}
