package com.mars.module.oss.factory;

import com.mars.module.oss.common.enums.FileUploadTypeEnums;
import com.mars.module.oss.strategy.Strategy;
import org.springframework.util.ObjectUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 策略工厂
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-08-17 09:28:27
 */
public class StrategyFactory {

    private static final Map<FileUploadTypeEnums, Strategy> STRATEGY_MAP = new ConcurrentHashMap<>();

    /**
     * 通过类型获取具体的策略
     *
     * @param type 名称
     * @return 策略
     */
    public static Strategy getStrategy(FileUploadTypeEnums type) {
        return STRATEGY_MAP.get(type);
    }

    /**
     * 注册策略
     *
     * @param type    类型名称
     * @param handler 策略接口
     */
    public static void register(FileUploadTypeEnums type, Strategy handler) {
        if (ObjectUtils.isEmpty(type) && handler == null) {
            return;
        }
        STRATEGY_MAP.put(type, handler);
    }
}
