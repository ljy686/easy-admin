package com.mars.module.tool.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.mars.module.system.entity.BaseEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单权限表 sys_menu
 *
 * @author mars
 */
@Data
@TableName(value = "sys_menu")
public class GenSysMenu extends BaseEntity {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    private String menuName;

    /**
     * 1.目录 2 菜单 3 按钮
     */
    private Integer menuType;
    /**
     * URL
     */
    private String url;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 图标
     */
    private String icon;

    /**
     * 备注
     */
    private String remark;

    /**
     * 基于哪个表生成的菜单
     */
    private String tableSource;


    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<GenSysMenu> children = new ArrayList<GenSysMenu>();

}
