package com.mars.common.request.tool;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 表查询
 *
 * @author 源码字节-程序员Mars
 */
@Data
@ApiModel(value = "请求参数")
@EqualsAndHashCode(callSuper = true)
public class TableQueryRequest extends PageRequest {

    @ApiModelProperty(value = "表名称")
    private String tableName;

    @ApiModelProperty(value = "表描述")
    private String tableComment;

    @JsonIgnore
    @ApiModelProperty(value = "系统表")
    private List<String> sysTableList;

}
