package ${packageName}.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import ${packageName}.entity.${ClassName};
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import ${packageName}.service.I${ClassName}Service;
import ${packageName}.request.${ClassName}Request;

import javax.servlet.http.HttpServletResponse;

/**
 * ${functionName}控制层
 *
 * @author ${author}
 * @date ${datetime}
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "${functionName}接口管理",tags = "${functionName}接口管理")
@RequestMapping("/${moduleName}/${jsName}" )
public class ${ClassName}Controller {

    private final I${ClassName}Service i${ClassName}Service;

    /**
     * 分页查询${functionName}列表
     */
    @ApiOperation(value = "分页查询${functionName}列表")
    @PostMapping("/pageList")
    public R<PageInfo<${ClassName}>> pageList(@RequestBody ${ClassName}Request ${className}Request) {
        return R.success(i${ClassName}Service.pageList(${className}Request));
    }

    /**
     * 获取${functionName}详细信息
     */
    @ApiOperation(value = "获取${functionName}详细信息")
    @GetMapping(value = "/query/{${pkColumn.javaField}}")
    public R<${ClassName}> detail(@PathVariable("${pkColumn.javaField}") ${pkColumn.javaType} ${pkColumn.javaField}) {
        return R.success(i${ClassName}Service.getById(${pkColumn.javaField}));
    }

    /**
     * 新增${functionName}
     */
    @Log(title = "新增${functionName}", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增${functionName}")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ${ClassName}Request ${className}Request) {
        i${ClassName}Service.add(${className}Request);
        return R.success();
    }

    /**
     * 修改${functionName}
     */
    @Log(title = "修改${functionName}", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改${functionName}")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ${ClassName}Request ${className}Request) {
        i${ClassName}Service.update(${className}Request);
        return R.success();
    }

    /**
     * 删除${functionName}
     */
    @Log(title = "删除${functionName}", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除${functionName}")
    @PostMapping("/delete/{${pkColumn.javaField}s}")
    public R<Void> remove(@PathVariable ${pkColumn.javaType}[] ${pkColumn.javaField}s) {
        i${ClassName}Service.deleteBatch(Arrays.asList(${pkColumn.javaField}s));
        return R.success();
    }

    /**
    * 导出${functionName}
    *
    * @param response response
    * @throws IOException IOException
    */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody ${ClassName}Request ${className}Request) throws IOException {
        List<${ClassName}> list = i${ClassName}Service.list(${className}Request);
        ExcelUtils.exportExcel(list, ${ClassName}.class, "${functionName}信息", response);
    }
}
